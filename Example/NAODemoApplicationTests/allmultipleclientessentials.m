//
//  NAODemoApplicationTests.m
//  NAODemoApplicationTests
//
//  Created by PolestarMacPro on 09/02/2018.
//  Copyright © 2018 PoleStar. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreLocation/CoreLocation.h>
#import <NAOSDK/NAOSDK.h>
#define VALID_KEY @"oarWVFpQUbccjy7ZBZCaOQ"
#define WAIT_TIME 10.0f

@interface AllMultipleClientEssentials : XCTestCase<
NAOSyncDelegate,
NAOSensorsDelegate,
NAOLocationHandleDelegate,
NAOBeaconReportingHandleDelegate,
NAOAnalyticsHandleDelegate,
NAOGeofencingHandleDelegate,
NAOBeaconProximityHandleDelegate
>

@property (strong, nonatomic) NAOLocationHandle *locationHandle;
@property (strong, nonatomic) NAOGeofencingHandle *geofencingHandle;
@property (strong, nonatomic) NAOAnalyticsHandle *analyticsHandle;
@property (strong, nonatomic) NAOBeaconReportingHandle *reportingHandle;
@property (strong, nonatomic) NAOBeaconProximityHandle *proximityHandle;
@property (strong, nonatomic) XCTestExpectation *expectLocationSynchro;
@property (strong, nonatomic) XCTestExpectation *expectGeofencingSynchro;
@property (strong, nonatomic) XCTestExpectation *expectAnalyticsSynchro;
@property (strong, nonatomic) XCTestExpectation *expectReportingSynchro;
@property (strong, nonatomic) XCTestExpectation *expectProximitySynchro;
@property (strong, nonatomic) XCTestExpectation *expectLocationObtained;
@property (strong, nonatomic) XCTestExpectation *expectNoLocation;
@property (strong, nonatomic) XCTestExpectation *expectFireNaoAlert;
@property (strong, nonatomic) XCTestExpectation *expectRangeBeacon;
@property (strong, nonatomic) XCTestExpectation *expectProximityChanged;
@property (strong, nonatomic) XCTestExpectation *expectError;
@property BOOL isProxymityUpdated;

@end

@implementation AllMultipleClientEssentials

- (void)setUp {
    [super setUp];
    _isProxymityUpdated = NO;
    
    // Services initialization
    if (_locationHandle == nil) {
        _locationHandle = [[NAOLocationHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
    }
    if (_geofencingHandle == nil) {
        _geofencingHandle = [[NAOGeofencingHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
    }
    if (_proximityHandle == nil) {
        _proximityHandle = [[NAOBeaconProximityHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
    }
    if (_reportingHandle == nil) {
        _reportingHandle = [[NAOBeaconReportingHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
    }
    if (_analyticsHandle == nil) {
        _analyticsHandle = [[NAOAnalyticsHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
    }
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    [_analyticsHandle stop];
    [_geofencingHandle stop];
    [_locationHandle stop];
    [_proximityHandle stop];
    [_reportingHandle stop];
    [NSThread sleepForTimeInterval:3.0f];
}

- (void)testAllServices {
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);

    // Location service synchronization
    _expectLocationSynchro = [self expectationWithDescription:@"Expectation for location synchro success"];
    [_locationHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectLocationSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Geofencing service synchronization
    _expectGeofencingSynchro = [self expectationWithDescription:@"Expectation for geofencing synchro success"];
    [_geofencingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectGeofencingSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Analytics service synchronization
    _expectAnalyticsSynchro = [self expectationWithDescription:@"Expectation for analytics synchro success"];
    [_analyticsHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectAnalyticsSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Proximity service synchronization
    _expectProximitySynchro = [self expectationWithDescription:@"Expectation for proximity synchro success"];
    [_proximityHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectProximitySynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Beacon reporting service synchronization
    _expectReportingSynchro = [self expectationWithDescription:@"Expectation for reporting synchro success"];
    [_reportingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectReportingSynchro = nil;
    
    // Geofencing service start
    _expectFireNaoAlert = [self expectationWithDescription:@"Expectation for Fire Nao Alert"];
    if (![_geofencingHandle start]) {
        XCTFail(@"Could not start geofencing service");
    }
    [self waitForExpectationsWithTimeout:130.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Fire Nao Alert expected timeout error: %@", error);
        }
    }];
    _expectFireNaoAlert = nil;
    
    // Location service start
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:120.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    
    // Proximity service start
    _expectProximityChanged = [self expectationWithDescription:@"Expectation Proximity Changed"];
    if (![_proximityHandle start]) {
        XCTFail(@"Could not start proximity service");
    }
    [self waitForExpectationsWithTimeout:130.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon Proximity changed expected timeout error: %@", error);
        }
    }];
    _expectProximityChanged = nil;
    _expectRangeBeacon = [self expectationWithDescription:@"Expectation Range Beacon"];
    [self waitForExpectationsWithTimeout:130.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon range changed expected timeout error: %@", error);
        }
    }];
    _expectRangeBeacon = nil;
    
    // Analytics service start
    _expectError = [self expectationWithDescription:@"Expectation no error"];
    [_expectError setInverted:YES];
    if (![_analyticsHandle start]) {
        XCTFail(@"Could not start analytics service");
    }
    [self waitForExpectationsWithTimeout:2*WAIT_TIME handler:^(NSError *error) {
        if (error) {
            NSLog(@"Analytics no error expectation timeout: %@", error);
        }
    }];
    _expectError = nil;
}

- (void)testLocationGeofencing {
    // Location service synchronization
    _expectLocationSynchro = [self expectationWithDescription:@"Expectation for location synchro success"];
    [_locationHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectLocationSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Geofencing service synchronization
    _expectGeofencingSynchro = [self expectationWithDescription:@"Expectation for geofencing synchro success"];
    [_geofencingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectGeofencingSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Geofencing service start
    _expectFireNaoAlert = [self expectationWithDescription:@"Expectation for Fire Nao Alert"];
    if (![_geofencingHandle start]) {
        XCTFail(@"Could not start geofencing service");
    }
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Fire Nao Alert expected timeout error: %@", error);
        }
    }];
    _expectFireNaoAlert = nil;
    
    // Location service start
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    
    // Beacon reporting service start
    _expectError = [self expectationWithDescription:@"Expectation for no error"];
    if (![_reportingHandle start]) {
        XCTFail(@"Could not start beacon reporting service");
    }
    [_expectError setInverted:YES];
    [self waitForExpectationsWithTimeout:WAIT_TIME handler:^(NSError *error) {
        if (error) {
            NSLog(@"Reporting no error expected timeout error: %@", error);
        }
    }];
    _expectError = nil;
}

- (void)testLocationProximity {
    // Location service synchronization
    _expectLocationSynchro = [self expectationWithDescription:@"Expectation for location synchro success"];
    [_locationHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectLocationSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Proximity service synchronization
    _expectProximitySynchro = [self expectationWithDescription:@"Expectation for proximity synchro success"];
    [_proximityHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectProximitySynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Location service start
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    
    // Proximity service start
    _expectProximityChanged = [self expectationWithDescription:@"Expectation Proximity Changed"];
    if (![_proximityHandle start]) {
        XCTFail(@"Could not start proximity service");
    }
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon Proximity changed expected timeout error: %@", error);
        }
    }];
    _expectProximityChanged = nil;
    _expectRangeBeacon = [self expectationWithDescription:@"Expectation Range Beacon"];
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon range changed expected timeout error: %@", error);
        }
    }];
    _expectRangeBeacon = nil;
}

- (void)testLocationReporting {
    // Location service synchronization
    _expectLocationSynchro = [self expectationWithDescription:@"Expectation for location synchro success"];
    [_locationHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectLocationSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Beacon reporting service synchronization
    _expectReportingSynchro = [self expectationWithDescription:@"Expectation for reporting synchro success"];
    [_reportingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectReportingSynchro = nil;
    
    // Location service start
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    
    // Beacon reporting service start
    _expectError = [self expectationWithDescription:@"Expectation for no error"];
    [_expectError setInverted:YES];
    if (![_reportingHandle start]) {
        XCTFail(@"Could not start beacon reporting service");
    }
    [self waitForExpectationsWithTimeout:WAIT_TIME handler:^(NSError *error) {
        if (error) {
            NSLog(@"Reporting no error expected timeout error: %@", error);
        }
    }];
    _expectError = nil;
}

- (void)testGeofencingAnalytics {
    // Geofencing service synchronization
    _expectGeofencingSynchro = [self expectationWithDescription:@"Expectation for geofencing synchro success"];
    [_geofencingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectGeofencingSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Analytics service synchronization
    _expectAnalyticsSynchro = [self expectationWithDescription:@"Expectation for analytics synchro success"];
    [_analyticsHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectAnalyticsSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Geofencing service start
    _expectFireNaoAlert = [self expectationWithDescription:@"Expectation for Fire Nao Alert"];
    if (![_geofencingHandle start]) {
        XCTFail(@"Could not start geofencing service");
    }
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Fire Nao Alert expected timeout error: %@", error);
        }
    }];
    _expectFireNaoAlert = nil;
    
    // Analytics service start
    _expectError = [self expectationWithDescription:@"Expectation no error"];
    [_expectError setInverted:YES];
    if (![_analyticsHandle start]) {
        XCTFail(@"Could not start analytics service");
    }
    [self waitForExpectationsWithTimeout:2*WAIT_TIME handler:^(NSError *error) {
        if (error) {
            NSLog(@"Analytics no error expectation timeout: %@", error);
        }
    }];
    _expectError = nil;
    
    [_analyticsHandle stop];
    [_geofencingHandle stop];
}

- (void)testGeofencingProximity {
    // Geofencing service synchronization
    _expectGeofencingSynchro = [self expectationWithDescription:@"Expectation for geofencing synchro success"];
    [_geofencingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectGeofencingSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Proximity service synchronization
    _expectProximitySynchro = [self expectationWithDescription:@"Expectation for proximity synchro success"];
    [_proximityHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectProximitySynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Geofencing service start
    _expectFireNaoAlert = [self expectationWithDescription:@"Expectation for Fire Nao Alert"];
    if (![_geofencingHandle start]) {
        XCTFail(@"Could not start geofencing service");
    }
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Fire Nao Alert expected timeout error: %@", error);
        }
    }];
    _expectFireNaoAlert = nil;
    
    // Proximity service start
    _expectProximityChanged = [self expectationWithDescription:@"Expectation Proximity Changed"];
    if (![_proximityHandle start]) {
        XCTFail(@"Could not start proximity service");
    }
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon Proximity changed expected timeout error: %@", error);
        }
    }];
    _expectProximityChanged = nil;
    _expectRangeBeacon = [self expectationWithDescription:@"Expectation Range Beacon"];
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon range changed expected timeout error: %@", error);
        }
    }];
    _expectRangeBeacon = nil;
}

- (void)testGeofencingReporting {
    // Geofencing service synchronization
    _expectGeofencingSynchro = [self expectationWithDescription:@"Expectation for geofencing synchro success"];
    [_geofencingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectGeofencingSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Beacon reporting service synchronization
    _expectReportingSynchro = [self expectationWithDescription:@"Expectation for reporting synchro success"];
    [_reportingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectReportingSynchro = nil;
    
    // Geofencing service start
    _expectFireNaoAlert = [self expectationWithDescription:@"Expectation for Fire Nao Alert"];
    if (![_geofencingHandle start]) {
        XCTFail(@"Could not start geofencing service");
    }
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Fire Nao Alert expected timeout error: %@", error);
        }
    }];
    _expectFireNaoAlert = nil;
    
    // Beacon reporting service start
    _expectError = [self expectationWithDescription:@"Expectation for no error"];
    [_expectError setInverted:YES];
    if (![_reportingHandle start]) {
        XCTFail(@"Could not start beacon reporting service");
    }
    [self waitForExpectationsWithTimeout:WAIT_TIME handler:^(NSError *error) {
        if (error) {
            NSLog(@"Reporting no error expected timeout error: %@", error);
        }
    }];
    _expectError = nil;
}

- (void)testAnalyticsProximity {
    // Analytics service synchronization
    _expectAnalyticsSynchro = [self expectationWithDescription:@"Expectation for analytics synchro success"];
    [_analyticsHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectAnalyticsSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Proximity service synchronization
    _expectProximitySynchro = [self expectationWithDescription:@"Expectation for proximity synchro success"];
    [_proximityHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectProximitySynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Proximity service start
    _expectProximityChanged = [self expectationWithDescription:@"Expectation Proximity Changed"];
    if (![_proximityHandle start]) {
        XCTFail(@"Could not start proximity service");
    }
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon Proximity changed expected timeout error: %@", error);
        }
    }];
    _expectProximityChanged = nil;
    _expectRangeBeacon = [self expectationWithDescription:@"Expectation Range Beacon"];
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon range changed expected timeout error: %@", error);
        }
    }];
    _expectRangeBeacon = nil;
    
    // Analytics service start
    _expectError = [self expectationWithDescription:@"Expectation no error"];
    if (![_analyticsHandle start]) {
        XCTFail(@"Could not start analytics service");
    }
    [_expectError setInverted:YES];
    [self waitForExpectationsWithTimeout:2*WAIT_TIME handler:^(NSError *error) {
        if (error) {
            NSLog(@"Analytics no error expectation timeout: %@", error);
        }
    }];
    _expectError = nil;
}

- (void)testAnalyticsReporting {
    // Analytics service synchronization
    _expectAnalyticsSynchro = [self expectationWithDescription:@"Expectation for analytics synchro success"];
    [_analyticsHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectAnalyticsSynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Beacon reporting service synchronization
    _expectReportingSynchro = [self expectationWithDescription:@"Expectation for reporting synchro success"];
    [_reportingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectReportingSynchro = nil;
    
    // Analytics service start
    _expectError = [self expectationWithDescription:@"Expectation no error"];
    if (![_analyticsHandle start]) {
        XCTFail(@"Could not start analytics service");
    }
    [_expectError setInverted:YES];
    [self waitForExpectationsWithTimeout:2*WAIT_TIME handler:^(NSError *error) {
        if (error) {
            NSLog(@"Analytics no error expectation timeout: %@", error);
        }
    }];
    _expectError = nil;
    
    // Beacon reporting service start
    _expectError = [self expectationWithDescription:@"Expectation for no error"];
    [_expectError setInverted:YES];
    if (![_reportingHandle start]) {
        XCTFail(@"Could not start beacon reporting service");
    }
    [self waitForExpectationsWithTimeout:WAIT_TIME handler:^(NSError *error) {
        if (error) {
            NSLog(@"Reporting no error expected timeout error: %@", error);
        }
    }];
    _expectError = nil;
}

- (void)testProximityReporting {
    // Proximity service synchronization
    _expectProximitySynchro = [self expectationWithDescription:@"Expectation for proximity synchro success"];
    [_proximityHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectProximitySynchro = nil;
    [NSThread sleepForTimeInterval:WAIT_TIME];
    
    // Beacon reporting service synchronization
    _expectReportingSynchro = [self expectationWithDescription:@"Expectation for reporting synchro success"];
    [_reportingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectReportingSynchro = nil;
    
    // Proximity service start
    _expectProximityChanged = [self expectationWithDescription:@"Expectation Proximity Changed"];
    if (![_proximityHandle start]) {
        XCTFail(@"Could not start proximity service");
    }
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon Proximity changed expected timeout error: %@", error);
        }
    }];
    _expectProximityChanged = nil;
    _expectRangeBeacon = [self expectationWithDescription:@"Expectation Range Beacon"];
    [self waitForExpectationsWithTimeout:30.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon range changed expected timeout error: %@", error);
        }
    }];
    _expectRangeBeacon = nil;
    
    // Beacon reporting service start
    _expectError = [self expectationWithDescription:@"Expectation for no error"];
    [_expectError setInverted:YES];
    if (![_reportingHandle start]) {
        XCTFail(@"Could not start beacon reporting service");
    }
    [self waitForExpectationsWithTimeout:WAIT_TIME handler:^(NSError *error) {
        if (error) {
            NSLog(@"Reporting no error expected timeout error: %@", error);
        }
    }];
    _expectError = nil;
}

// Callbacks

#pragma mark - NAOLocationHandleDelegate

- (void) didLocationChange:(CLLocation *)location {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [location description]);
    if (_expectLocationObtained != nil) {
        [_expectLocationObtained fulfill];
    }
}

- (void) didFailWithErrorCode:(DBNAOERRORCODE)errCode andMessage:(NSString *)message {
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
}

- (void) didLocationStatusChanged:(DBTNAOFIXSTATUS)status {
    NSString* statusString = [self stringFromStatus:status];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), statusString);
}


- (NSString*) stringFromStatus:(DBTNAOFIXSTATUS) status {
    switch (status) {
        case DBTNAOFIXSTATUS_NAO_FIX_UNAVAILABLE:
            return @"FIX_UNAVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_FIX_AVAILABLE:
            return @"AVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_TEMPORARY_UNAVAILABLE:
            return @"TEMPORARY_UNAVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_OUT_OF_SERVICE:
            return @"OUT_OF_SERVICE";
            break;
        default:
            return @"UNKNOWN";
            break;
    }
}

- (void) didEnterSite:(NSString *)name {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), name);
}

- (void) didExitSite:(NSString *)name {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), name);
}

#pragma mark - NAOBeaconProximityHandleDelegate

- (void)didProximityChange:(DBTBEACONSTATE)proximity forBeacon:(NSString *)beaconPublicID {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), beaconPublicID);
    _isProxymityUpdated = YES;
    if (_expectProximityChanged != nil) {
        [_expectProximityChanged fulfill];
    }
}

- (void)didRangeBeacon:(NSString *)beaconPublicID withRssi:(int)rssi {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), beaconPublicID);
    if (_expectRangeBeacon != nil) {
        [_expectRangeBeacon fulfill];
    }
}

- (void)didFireNAOAlert:(NaoAlert *)alert {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [alert description]);
    if (_expectFireNaoAlert != nil) {
        [_expectFireNaoAlert fulfill];
    }
}

#pragma mark - NAOSyncDelegate

- (void)  didSynchronizationSuccess {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    if (_expectReportingSynchro != nil) {
        [_expectReportingSynchro fulfill];
    }
    if (_expectProximitySynchro != nil) {
        [_expectProximitySynchro fulfill];
    }
    if (_expectAnalyticsSynchro != nil) {
        [_expectAnalyticsSynchro fulfill];
    }
    if (_expectGeofencingSynchro != nil) {
        [_expectGeofencingSynchro fulfill];
    }
    if (_expectLocationSynchro != nil) {
        [_expectLocationSynchro fulfill];
    }
}

- (void)  didSynchronizationFailure:(DBNAOERRORCODE)errorCode msg:(NSString*) message {
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errorCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
    XCTFail(@"SynchroFailure %@", message);
    if (_expectError != nil) {
        [_expectError fulfill];
    }
}

#pragma mark - NAOSensorsDelegate

- (void)requiresBLEOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresWifiOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresLocationOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresCompassCalibration {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

//Optional
- (void)didCompassCalibrated {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

@end
