//
//  NAODemoApplicationTests.m
//  NAODemoApplicationTests
//
//  Created by PolestarMacPro on 09/02/2018.
//  Copyright © 2018 PoleStar. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreLocation/CoreLocation.h>
#import <NAOSDK/NAOSDK.h>
#define VALID_KEY @"oarWVFpQUbccjy7ZBZCaOQ"

@interface ReportingEssentials : XCTestCase<NAOSyncDelegate, NAOSensorsDelegate, NAOBeaconReportingHandleDelegate>


@property (strong, nonatomic) NAOBeaconReportingHandle *reportingHandle;
@property (strong, nonatomic) XCTestExpectation *expectSynchroSuccess;
@property (strong, nonatomic) XCTestExpectation *expectError;

@end

@implementation ReportingEssentials

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testBasicReporting {
    _reportingHandle = [[NAOBeaconReportingHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_reportingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    
    _expectError = [self expectationWithDescription:@"Expectation no error"];
    [_expectError setInverted:YES];
    if (![_reportingHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"No error expected timeout error: %@", error);
        }
    }];
    [_reportingHandle stop];
}

// Callbacks

- (void) didFailWithErrorCode:(DBNAOERRORCODE)errCode andMessage:(NSString*)message {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [message description]);
    [_expectError fulfill];
}

#pragma mark - NAOSyncDelegate

- (void)  didSynchronizationSuccess {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    if (_expectSynchroSuccess != nil) {
        [_expectSynchroSuccess fulfill];
    }
}

- (void)  didSynchronizationFailure:(DBNAOERRORCODE)errorCode msg:(NSString*) message {
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errorCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
    XCTFail(@"SynchroFailure %@", message);
}

#pragma mark - NAOSensorsDelegate

- (void)requiresBLEOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresWifiOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresLocationOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresCompassCalibration {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

//Optional
- (void)didCompassCalibrated {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

@end
