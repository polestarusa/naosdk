//
//  NAODemoApplicationTests.m
//  NAODemoApplicationTests
//
//  Created by PolestarMacPro on 09/02/2018.
//  Copyright © 2018 PoleStar. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreLocation/CoreLocation.h>
#import <NAOSDK/NAOSDK.h>
#define VALID_KEY @"oarWVFpQUbccjy7ZBZCaOQ"

@interface ProximityEssentials : XCTestCase<NAOSyncDelegate, NAOSensorsDelegate, NAOBeaconProximityHandleDelegate>


@property (strong, nonatomic) NAOBeaconProximityHandle *proximityHandle;
@property (strong, nonatomic) XCTestExpectation *expectSynchroSuccess;
@property (strong, nonatomic) XCTestExpectation *expectLocationObtained;
@property (strong, nonatomic) XCTestExpectation *expectNoLocation;
@property (strong, nonatomic) XCTestExpectation *expectFireNaoAlert;
@property (strong, nonatomic) XCTestExpectation *expectRangeBeacon;
@property (strong, nonatomic) XCTestExpectation *expectProximityChanged;

@end

@implementation ProximityEssentials

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testBasicProximity {
    _proximityHandle = [[NAOBeaconProximityHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_proximityHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    
    _expectProximityChanged = [self expectationWithDescription:@"Expectation Proximity Changed"];
    if (![_proximityHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:130.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon Proximity changed expected timeout error: %@", error);
        }
    }];
    _expectRangeBeacon = [self expectationWithDescription:@"Expectation Range Beacon"];
    [self waitForExpectationsWithTimeout:130.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Beacon range changed expected timeout error: %@", error);
        }
    }];
    
    [_proximityHandle stop];
}

// Callbacks

#pragma mark - NAOBeaconProximityHandleDelegate

- (void)didProximityChange:(DBTBEACONSTATE)proximity forBeacon:(NSString *)beaconPublicID {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), beaconPublicID);
    [_expectProximityChanged fulfill];
}

- (void)didRangeBeacon:(NSString *)beaconPublicID withRssi:(int)rssi {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), beaconPublicID);
    [_expectRangeBeacon fulfill];
}

- (void)didFireNAOAlert:(NaoAlert *)alert {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [alert description]);
    [_expectFireNaoAlert fulfill];
}

- (void) didFailWithErrorCode:(DBNAOERRORCODE)errCode andMessage:(NSString*)message {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [message description]);
}

#pragma mark - NAOSyncDelegate

- (void)  didSynchronizationSuccess {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    if (_expectSynchroSuccess != nil) {
        [_expectSynchroSuccess fulfill];
    }
}

- (void)  didSynchronizationFailure:(DBNAOERRORCODE)errorCode msg:(NSString*) message {
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errorCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
    XCTFail(@"SynchroFailure %@", message);
}

#pragma mark - NAOSensorsDelegate

- (void)requiresBLEOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresWifiOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresLocationOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresCompassCalibration {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

//Optional
- (void)didCompassCalibrated {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

@end
