//
//  NAODemoApplicationTests.m
//  NAODemoApplicationTests
//
//  Created by PolestarMacPro on 09/02/2018.
//  Copyright © 2018 PoleStar. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreLocation/CoreLocation.h>
#import <NAOSDK/NAOSDK.h>
#define VALID_KEY @"oarWVFpQUbccjy7ZBZCaOQ"

@interface LocationEssentials : XCTestCase<CLLocationManagerDelegate, NAOLocationHandleDelegate, NAOSyncDelegate, NAOSensorsDelegate>

@property (strong, nonatomic) NAOLocationHandle *locationHandle;
@property (weak, nonatomic) XCTestExpectation *expectCoreLocation;
@property (weak, nonatomic) XCTestExpectation *expectSynchroSuccess;
@property (weak, nonatomic) XCTestExpectation *expectLocationObtained;
@property (weak, nonatomic) XCTestExpectation *expectNoLocation;
@property (weak, nonatomic) XCTestExpectation *expectLocationStatusChanged;

@end

@implementation LocationEssentials

- (void)setUp {
    [super setUp];
    if (_locationHandle == nil) {
        _locationHandle = [[NAOLocationHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
    }
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _expectNoLocation = nil;
    _expectLocationObtained = nil;
    _expectLocationStatusChanged = nil;
    _expectCoreLocation = nil;
    _expectSynchroSuccess = nil;
    [_locationHandle stop];
    [NSThread sleepForTimeInterval:3.0];
}

- (void)testCoreLoc {
    CLLocationManager *locManager = [[CLLocationManager alloc] init];
    locManager.delegate = self;
    [locManager requestWhenInUseAuthorization];
    _expectCoreLocation = [self expectationWithDescription:@"Location expected from CLLocation manager"];
    [locManager startUpdatingLocation];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        NSLog(@"Location expected timeout error: %@", error);
    }];
    [locManager stopUpdatingLocation];
    locManager = nil;
}


- (void)testBasic {
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_locationHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:120.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    
    [_locationHandle stop];
}

- (void)testBasicTwice {
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_locationHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    
    for (NSUInteger i=0; i<2; ++i) {
        _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
        if (![_locationHandle start]) {
            XCTFail(@"Could not start location service");
        }
        [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
            if (error) {
                NSLog(@"Location expected timeout error: %@", error);
            }
        }];
    }
    [_locationHandle stop];
}

- (void)testOffline {
    
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    
    [_locationHandle stop];
}

- (void)testOnline {
    
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_locationHandle synchronizeData:self];
    
    [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    
    [_locationHandle stop];
}

- (void)testOnlineWithoutWaitingAfterSync {
    // Handle init
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    
    [_locationHandle synchronizeData:self];
    
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    
    [_locationHandle stop];
}

- (void)testOnlineWithoutWaitingAfterSyncStop {
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    
    // Sync
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_locationHandle synchronizeData:self];

    // Start loc without waiting for sync success
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    
    // Sleep for 1s
    [NSThread sleepForTimeInterval:1.0f];

    // Stop loc
    [_locationHandle stop];
    [NSThread sleepForTimeInterval:3.0f];
    
    // Wait for sync success but no locations
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for NO location obtained from Nao services"];
    [_expectLocationObtained setInverted:YES];
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    
    [_locationHandle stop];
}

- (void)testOfflineThenOnline {
    
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    
    [_locationHandle stop];
    [NSThread sleepForTimeInterval:3.0];
    
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_locationHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    
    [_locationHandle stop];
}

- (void)testDataBaseVersion {
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    
    NSString *dbVersions = [_locationHandle getDatabaseVersions];
    if (dbVersions == nil || dbVersions.length == 0) {
        XCTFail(@"PDB versions must not be empty nor nil");
    }
    NSLog(@"PDB versions retrieved : %@", dbVersions);
    
    [_locationHandle stop];
}

- (void) testSetPowerMode {
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_locationHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    
    [_locationHandle setPowerMode:DBTPOWERMODE_LOW];
    if ([_locationHandle getPowerMode] != DBTPOWERMODE_LOW) {
        XCTFail(@"Powermode should be DBTPOWERMODE_LOW");
    }
    
    [_locationHandle setPowerMode:DBTPOWERMODE_HIGH];
    if ([_locationHandle getPowerMode] != DBTPOWERMODE_HIGH) {
        XCTFail(@"Powermode should be DBTPOWERMODE_HIGH");
    }
    
    [_locationHandle stop];
}

- (void)testSoftwareVersion {
    NSString *softwareVersion = [[NAOServicesConfig getNaoContext] getSoftwareVersion];
    NSLog(@"SDK version : %@", softwareVersion);
    if (softwareVersion.length == 0 || softwareVersion == nil) {
        XCTFail(@"SDK Version should not be empty nor nil");
    }
}

- (void)testSyncAfterOtherSync {
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_locationHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_locationHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:1.0f handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    
    [_locationHandle stop];
}

- (void) testStartAfterOtherStart {
    // Location start
    _expectLocationStatusChanged = [self expectationWithDescription:@"Expectation for location status changed"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationStatusChanged = nil;
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    
    // Location start again
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    
    [_locationHandle stop];
}

- (void)testStopWithoutStart {
    // Stop without starting beforehand
    [_locationHandle stop];
}

- (void)testStartAfterStop {
    // Location start
    _expectLocationStatusChanged = [self expectationWithDescription:@"Expectation for location status changed"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationStatusChanged = nil;
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    
    // Location stop
    [_locationHandle stop];
    [NSThread sleepForTimeInterval:3.0f];
    
    // Location start again
    _expectLocationStatusChanged = [self expectationWithDescription:@"Expectation for location status changed"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationStatusChanged = nil;
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
}

- (void)testStartAfterStopDelay {
    // Location start
    _expectLocationStatusChanged = [self expectationWithDescription:@"Expectation for location status changed"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationStatusChanged = nil;
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    
    // Location stop
    [_locationHandle stop];
    
    // Wait for 5s
    [NSThread sleepForTimeInterval:5.0f];
    
    // Location start again
    _expectLocationStatusChanged = [self expectationWithDescription:@"Expectation for location status changed"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationStatusChanged = nil;
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    _locationHandle = nil;
}

- (void)testStartAfterDelayAfterStarStop {
    // Location start
    _expectLocationStatusChanged = [self expectationWithDescription:@"Expectation for location status changed"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationStatusChanged = nil;
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    
    // Location stop
    [_locationHandle stop];
    
    // Location start again
    _expectLocationStatusChanged = [self expectationWithDescription:@"Expectation for location status changed"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationStatusChanged = nil;
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
    
    // Wait for 5s
    [NSThread sleepForTimeInterval:5.0f];
    
    // Location start again
    _expectLocationStatusChanged = [self expectationWithDescription:@"Expectation for location status changed"];
    if (![_locationHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationStatusChanged = nil;
    _expectLocationObtained = [self expectationWithDescription:@"Expectation for location obtained from Nao services"];
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    _expectLocationObtained = nil;
}

//- (void)testLocationFormat { // <-- not relevant on iOS?
//
//}

// Callbacks

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    NSLog(@"Location received from CLLocation manager!");
    if (_expectCoreLocation != nil) {
        [_expectCoreLocation fulfill];
    }
}


#pragma mark - NAOLocationHandleDelegate

- (void) didLocationChange:(CLLocation *)location {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [location description]);
    if (_expectLocationObtained != nil) {
        [_expectLocationObtained fulfill];
        _expectLocationObtained = nil;
    }
}

- (void) didFailWithErrorCode:(DBNAOERRORCODE)errCode andMessage:(NSString *)message {
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
}

- (void) didLocationStatusChanged:(DBTNAOFIXSTATUS)status {
    NSString* statusString = [self stringFromStatus:status];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), statusString);
    if (_expectLocationStatusChanged != nil) {
        [_expectLocationStatusChanged fulfill];
    }
}


- (NSString*) stringFromStatus:(DBTNAOFIXSTATUS) status {
    switch (status) {
        case DBTNAOFIXSTATUS_NAO_FIX_UNAVAILABLE:
            return @"FIX_UNAVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_FIX_AVAILABLE:
            return @"AVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_TEMPORARY_UNAVAILABLE:
            return @"TEMPORARY_UNAVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_OUT_OF_SERVICE:
            return @"OUT_OF_SERVICE";
            break;
        default:
            return @"UNKNOWN";
            break;
    }
}

- (void) didEnterSite:(NSString *)name {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), name);
}

- (void) didExitSite:(NSString *)name {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), name);
}

#pragma mark - NAOSyncDelegate

- (void)  didSynchronizationSuccess {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    if (_expectSynchroSuccess != nil) {
        [_expectSynchroSuccess fulfill];
    }
}

- (void)  didSynchronizationFailure:(DBNAOERRORCODE)errorCode msg:(NSString*) message {
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errorCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
    XCTFail(@"SynchroFailure %@", message);
}

#pragma mark - NAOSensorsDelegate

- (void)requiresBLEOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresWifiOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresLocationOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresCompassCalibration {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

//Optional
- (void)didCompassCalibrated {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

@end
