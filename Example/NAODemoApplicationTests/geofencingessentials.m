//
//  NAODemoApplicationTests.m
//  NAODemoApplicationTests
//
//  Created by PolestarMacPro on 09/02/2018.
//  Copyright © 2018 PoleStar. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreLocation/CoreLocation.h>
#import <NAOSDK/NAOSDK.h>
#define VALID_KEY @"oarWVFpQUbccjy7ZBZCaOQ"

@interface GeofencingEssentials : XCTestCase<NAOSyncDelegate, NAOSensorsDelegate, NAOGeofencingHandleDelegate>


@property (strong, nonatomic) NAOLocationHandle *locationHandle;
@property (strong, nonatomic) NAOGeofencingHandle *geofencingHandle;
@property (strong, nonatomic) XCTestExpectation *expectSynchroSuccess;
@property (strong, nonatomic) XCTestExpectation *expectLocationObtained;
@property (strong, nonatomic) XCTestExpectation *expectNoLocation;
@property (strong, nonatomic) XCTestExpectation *expectFireNaoAlert;
@property (strong, nonatomic) XCTestExpectation *expectError;

@end

@implementation GeofencingEssentials

- (void)setUp {
    [super setUp];
    if (_geofencingHandle == nil) {
        _geofencingHandle = [[NAOGeofencingHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
    }

    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _expectSynchroSuccess = nil;
    _expectNoLocation = nil;
    _expectLocationObtained = nil;
    _expectError = nil;
    _expectFireNaoAlert = nil;
    [_geofencingHandle stop];
    [NSThread sleepForTimeInterval:3.0f];
}

- (void)testBasicFineGeofence {
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_geofencingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    _expectFireNaoAlert = [self expectationWithDescription:@"Expectation for Fire Nao Alert"];
    if (![_geofencingHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:130.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Location expected timeout error: %@", error);
        }
    }];
    [_geofencingHandle stop];
}

- (void) testSetPowerMode {
    _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
    [_geofencingHandle synchronizeData:self];
    [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Sync timeout Error: %@", error);
        }
    }];
    
    _expectFireNaoAlert = [self expectationWithDescription:@"Expectation for Fire Nao alert from Nao services"];
    if (![_geofencingHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:20.0 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Fire Nao Alert expected timeout error: %@", error);
        }
    }];
    [_geofencingHandle setPowerMode:DBTPOWERMODE_LOW];
    if ([_geofencingHandle getPowerMode] != DBTPOWERMODE_LOW) {
        XCTFail(@"Powermode should be DBTPOWERMODE_LOW");
    }
    [_geofencingHandle setPowerMode:DBTPOWERMODE_HIGH];
    if ([_geofencingHandle getPowerMode] != DBTPOWERMODE_HIGH) {
        XCTFail(@"Powermode should be DBTPOWERMODE_HIGH");
    }
    [_geofencingHandle stop];
}

- (void)testOnlineWithoutWaitingAfterSync {
    NSLog(@"NAODemoApp : %@ : %@ : Version : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NAOServicesConfig getSoftwareVersion]);
    [_geofencingHandle synchronizeData:self];
    
    _expectFireNaoAlert = [self expectationWithDescription:@"Expectation for Fire Nao Alert"];
    if (![_geofencingHandle start]) {
        XCTFail(@"Could not start location service");
    }
    [self waitForExpectationsWithTimeout:50 handler:^(NSError *error) {
        if (error) {
            NSLog(@"Fire Nao Alert timeout error: %@", error);
        }
    }];
    [_geofencingHandle stop];
}

- (void) testDualClientRegister {
    @autoreleasepool {
        // Geofencing handles init
        NAOGeofencingHandle *geofencingHandle1 = [[NAOGeofencingHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
        NAOGeofencingHandle *geofencingHandle2 = [[NAOGeofencingHandle alloc] initWithKey:VALID_KEY delegate:self sensorsDelegate:self];
        
        // Synchro
        _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
        [geofencingHandle1 synchronizeData:self];
        [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
            if (error) {
                NSLog(@"Sync timeout Error: %@", error);
            }
        }];
        _expectSynchroSuccess = [self expectationWithDescription:@"Expectation for synchro success"];
        [geofencingHandle2 synchronizeData:self];
        [self waitForExpectationsWithTimeout:50.0 handler:^(NSError *error) {
            if (error) {
                NSLog(@"Sync timeout Error: %@", error);
            }
        }];
        
        // Geofencing start, expecting no errors for 10s
        _expectError = [self expectationWithDescription:@"Expectation for no error"];
        [_expectError setInverted:YES];
        if (![geofencingHandle1 start]) {
            XCTFail(@"Could not start geofencing service 1");
        }
        if (![geofencingHandle2 start]) {
            XCTFail(@"Could not start geofencing service 2");
        }
        [self waitForExpectationsWithTimeout:10.0 handler:^(NSError *error) {
            if (error) {
                NSLog(@"Error occured before timeout: %@", error);
            }
        }];
        
        [geofencingHandle1 stop];
        [geofencingHandle2 stop];
        geofencingHandle1 = nil;
        geofencingHandle2 = nil;
        [NSThread sleepForTimeInterval:3.0];
    }
}

// Callbacks

#pragma mark - NAOGeofencingHandleDelegate

- (void)didFireNAOAlert:(NaoAlert *)alert {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [alert description]);
    if (_expectFireNaoAlert != nil) {
        [_expectFireNaoAlert fulfill];
    }
}

- (void) didFailWithErrorCode:(DBNAOERRORCODE)errCode andMessage:(NSString*)message {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [message description]);
    [_expectError fulfill];
}

#pragma mark - NAOSyncDelegate

- (void)  didSynchronizationSuccess {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    if (_expectSynchroSuccess != nil) {
        [_expectSynchroSuccess fulfill];
    }
}

- (void)  didSynchronizationFailure:(DBNAOERRORCODE)errorCode msg:(NSString*) message {
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errorCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
    XCTFail(@"SynchroFailure %@", message);
}

#pragma mark - NAOSensorsDelegate

- (void)requiresBLEOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresWifiOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresLocationOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresCompassCalibration {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

//Optional
- (void)didCompassCalibrated {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

@end
