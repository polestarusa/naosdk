//
//  ICallbackHelper.h
//  NAODemoApplication
//
//  Created by Pole Star on 27/02/2020.
//  Copyright © 2020 Pole Star. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NAOServicesConfig.h"

NS_ASSUME_NONNULL_BEGIN

@interface ICallbackHelper : NSObject <PSTICallback>

@end

NS_ASSUME_NONNULL_END
