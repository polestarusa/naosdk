//
//  ApplicationsTableViewController.m
//  NAODemoApplication
//
//  Created by Pole Star on 26/11/2015.
//  Copyright © 2015 Pole Star. All rights reserved.
//

#import "ApplicationsTableViewController.h"
#import "IndoorLocationViewController.h"
#import "ApiKeySelectionViewController.h"
#import "ICallbackHelper.h"


@interface ApplicationsTableViewController ()

@end

@implementation ApplicationsTableViewController

@synthesize services;
@synthesize indoorLocationService, geofencingService, beaconProximityService, analyticsService, beaconReportingService, wakeService, allServices,trackingService,uploadNaoLogInfo, speedDashboard;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    services = [[NSMutableArray alloc] init];
    indoorLocationService = @"IndoorLocation";
    geofencingService = @"Geofencing";
    beaconProximityService = @"BeaconProximity";
    analyticsService = @"Analytics";
    beaconReportingService = @"BeaconReporting";
    wakeService = @"WakeService";
    trackingService = @"Tracking service";
    allServices = @"AllServices";
    uploadNaoLogInfo = @"UplaodNaoLogInfo";
    speedDashboard = @"Speed dashboard";
    

    [services addObject:indoorLocationService];
    [services addObject:geofencingService];
    [services addObject:beaconProximityService];
    [services addObject:analyticsService];
    [services addObject:beaconReportingService];
    [services addObject:wakeService];
    [services addObject:trackingService];
    [services addObject:allServices];
    [services addObject:uploadNaoLogInfo];
    [services addObject:speedDashboard];
    
    NSString *apiKey = [ApiKeySelectionViewController getApikeyFromPref];
    NSLog(@"Default apiKey value: %@",apiKey);
    
    if (apiKey == nil) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:DEFAULT_KEY_VALUE forKey:@"apiKey"];
        [userDefaults synchronize];
    }
    else{
        // Force the last valide to be reused
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:apiKey forKey:@"apiKey"];
        [userDefaults synchronize];
    }
    
    self.title = @"Services";
}

/** Append text to a file located in the /Documents directory.

 If file does not exist it will be created from scratch.

 @param str String to write.
 @param fileName Name of the file.

 */
+ (void)writeAndAppendString:(NSString *)str toFile:(NSString *)filePath {

    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]) {
        // Add the text at the end of the file.
        NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
        [fileHandler seekToEndOfFile];
        [fileHandler writeData:data];
        [fileHandler closeFile];
    } else {
        // Create the file and write text to it.
        [data writeToFile:filePath atomically:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onChangeApikeyButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"apiKeySelectionSegue" sender:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [services count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = [services objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if ([[self.services objectAtIndex:indexPath.row] isEqualToString:indoorLocationService]) {
        NSLog(@"%@",indoorLocationService);
        [self performSegueWithIdentifier:@"IndoorLocationSegue" sender:self];
    }
    
    if ([[self.services objectAtIndex:indexPath.row] isEqualToString:geofencingService]) {
        NSLog(@"%@",geofencingService);
        [self performSegueWithIdentifier:@"GeofencingSegue" sender:self];
    }
    
    if ([[self.services objectAtIndex:indexPath.row] isEqualToString:beaconProximityService]) {
        NSLog(@"%@",beaconProximityService);
        [self performSegueWithIdentifier:@"BeaconProximitySegue" sender:self];
    }
    
    if ([[self.services objectAtIndex:indexPath.row] isEqualToString:analyticsService]) {
        NSLog(@"%@",analyticsService);
        [self performSegueWithIdentifier:@"AnalyticsSegue" sender:self];
    }
    
    if ([[self.services objectAtIndex:indexPath.row] isEqualToString:beaconReportingService]) {
        NSLog(@"%@",beaconReportingService);
        [self performSegueWithIdentifier:@"BeaconReportingSegue" sender:self];
    }
    
    if ([[self.services objectAtIndex:indexPath.row] isEqualToString:wakeService]) {
        NSLog(@"%@",wakeService);
        [self performSegueWithIdentifier:@"WakeServiceMainSegue" sender:self];
    }
    
    if ([[self.services objectAtIndex:indexPath.row] isEqualToString:trackingService]) {
        NSLog(@"%@",wakeService);
        [self performSegueWithIdentifier:@"TrackingServiceViewControllerSegue" sender:self];
    }
    
    if ([[self.services objectAtIndex:indexPath.row] isEqualToString:allServices]) {
        NSLog(@"%@",allServices);
        [self performSegueWithIdentifier:@"AllServicesSegue" sender:self];
    }
    
    if ([[self.services objectAtIndex:indexPath.row] isEqualToString:speedDashboard]) {
        NSLog(@"%@", speedDashboard);
        [self performSegueWithIdentifier:@"SpeedDashboardSegue" sender:self];
    }
    
    if ([[self.services objectAtIndex:indexPath.row] isEqualToString:uploadNaoLogInfo]) {
        ICallbackHelper *callbackHelper = [[ICallbackHelper alloc] init];
        NSString *apikey = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastApiKey"];
        
        // Add the selected apikey
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:apikey forKey:@"apiKey"];
        [userDefaults synchronize];
        
        [NAOServicesConfig uploadNAOLogInfo:apikey callback:callbackHelper];
    }
}


@end
