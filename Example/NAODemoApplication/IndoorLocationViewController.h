//
//  IndoorLocationViewController.h
//  NAODemoApplication
//
//  Created by Pole Star on 27/11/2015.
//  Copyright © 2015 Pole Star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NAOSDK/NAOSDK.h>
#import "NotificationManager.h"


@interface IndoorLocationViewController : UIViewController<NAOLocationHandleDelegate, NAOSyncDelegate, NAOSensorsDelegate, UITextFieldDelegate>

@property NAOLocationHandle* locationHandle;

@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *synchronizeButton;
@property (weak, nonatomic) IBOutlet UIButton *restartButton;
@property (weak, nonatomic) IBOutlet UITextField *delaiNotificationTextField;
@property (weak, nonatomic) IBOutlet UITextField *selectedTextView;
@property (weak, nonatomic) IBOutlet UIButton *enableAutoPdbSyncButton;
@property (weak, nonatomic) IBOutlet UIButton *setExternalLocButton;
@property (weak, nonatomic) IBOutlet UILabel *naosdkVersionLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorConsoleLabel;
@property (weak, nonatomic) IBOutlet UIButton *lastLocationButton;
@property (weak, nonatomic) IBOutlet UILabel *locTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastLocConsoleLabel;
@property (weak, nonatomic) IBOutlet UIStackView *lastLocStackView;
@property NotificationManager *notificationManager;

@end
