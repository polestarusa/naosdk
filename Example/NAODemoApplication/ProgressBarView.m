//
//  ProgressBarView.m
//  NAODemoApplication
//
//  Created by Pole Star on 10/02/2022.
//  Copyright © 2022 David FERNANDEZ. All rights reserved.
//

#import "ProgressBarView.h"

@implementation ProgressBarView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)viewDidMoveToWindow{
    self.percent = 0;
//    attributes=[NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Helvetica" size:26],NSFontAttributeName,[NSColor redColor],NSForegroundColorAttributeName, nil];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];

        // Determine our start and stop angles for the arc (in radians)
        self.startAngle = M_PI * 1.5;
        self.endAngle = self.startAngle + (M_PI * 2);

    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    // Display our percentage as a string
    NSString* textContent = [NSString stringWithFormat:@"%d", self.percent];
//    NSLog(@"########\t[%d]\t%s\tpercent: %d",__LINE__, __FILE__ , self.percent);
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];

    // Create our arc, with the correct angles
    [bezierPath addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                          radius:130
                      startAngle:self.startAngle
                        endAngle:(self.endAngle - self.startAngle) * (self.percent / 100.0) + self.startAngle
                       clockwise:YES];

    // Set the display for the path, and stroke it
    bezierPath.lineWidth = 20;
    [[UIColor redColor] setStroke];
    [bezierPath stroke];

    // Text Drawing
    CGRect textRect = CGRectMake((rect.size.width / 2.0) - 71/2.0, (rect.size.height / 2.0) - 45/2.0, 71, 45);
    [[UIColor blackColor] setFill];
    
    [textContent drawInRect: textRect withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 42.5] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];
}

@end
