//
//  ApiKeySelectionViewController.m
//  NAODemoApplication
//
//  Created by Pole Star on 10/03/2016.
//  Copyright © 2016 Pole Star. All rights reserved.
//

#import "ApiKeySelectionViewController.h"
#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"

static UIColor *textview_background_color = nil;
static UIColor *textview_color = nil;

@interface ApiKeySelectionViewController ()

@end

@implementation ApiKeySelectionViewController

- (void)viewDidLoad {
    textview_background_color =  [UIColor colorWithRed:76.0/255.0
                                     green:76.0/255.0
                                      blue:76.0/255.0
                                     alpha:1.0];
    textview_color =  [UIColor colorWithRed: 0.78
                                      green: 0.78
                                       blue: 0.80
                                      alpha: 1.00];
    
    [super viewDidLoad];
    
    self.title = @"Api Key Selection";
    
    [self.apiKeyTextField setBackgroundColor:textview_color];
    self.apiKeyTextField.layer.borderColor=[textview_background_color CGColor];
    [self.apiKeySelectionTableView setDelegate:self];
    [self.apiKeySelectionTableView setDataSource:self];
    
    self.apiKeyList = [[NSMutableArray alloc] init];
    self.apiKeyList = [self getApikeyListFromPref];
    
    // Get apikey from pref
    NSString *lastApiKey = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastApiKey"];
    if (lastApiKey != nil) {
        self.apiKeyTextField.text = lastApiKey;
    }
    else if ([[NSUserDefaults standardUserDefaults] objectForKey:@"apiKey"] != nil) {
        self.apiKeyTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"apiKey"];
    }
    else if (self.apiKeyList != nil && [self.apiKeyList count] > 0) {
        self.apiKeyTextField.text = self.apiKeyList[0];
    }
    else {
        self.apiKeyTextField.placeholder = @"Enter or select your API Key";
    }
    
    // Add the selected apikey
    if (self.apiKeyTextField.text != nil){
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:self.apiKeyTextField.text forKey:@"lastApiKey"];
        [userDefaults synchronize];
    }
    
    [self.apiKeyTextField setReturnKeyType:UIReturnKeyDone];
    self.apiKeyTextField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[self addCurrentApikeyAlert];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.isMovingFromParentViewController) {
 
        // do something here if that's the case
        NSLog(@"Back button was pressed.");
        
        //[self saveCurrentApikeyAlert];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addCurrentApikeyAlert {
        self.alertController = [UIAlertController
                                    alertControllerWithTitle:@"Save apikey"
                                    message:@"Save the current apikey is not saved!"
                                    preferredStyle:UIAlertControllerStyleAlert];

       //Add Buttons

       UIAlertAction* yesButton = [UIAlertAction
                                   actionWithTitle:@"Save"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here
                                        [self appendApikeyIntoPref: self.apiKeyTextField.text];
                                   }];

       UIAlertAction* noButton = [UIAlertAction
                                  actionWithTitle:@"Cancel"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      //Continue
                                  }];

    [self.alertController addAction:yesButton];
    [self.alertController addAction:noButton];
}

- (void)saveCurrentApikeyAlert {
    if (![self search:self.apiKeyTextField.text]){
       [self presentViewController:self.alertController animated:YES completion:nil];
    }
}

- (IBAction)validateAction:(id)sender {
    [self appendApikeyIntoPref: self.apiKeyTextField.text];
    
    [self refresh];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - Table View Data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.apiKeyList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"apiKeyCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    UILabel *apiKeylabel = [cell viewWithTag:1];
    [apiKeylabel setText:[self.apiKeyList objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - TableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *apiKey = [self.apiKeyList objectAtIndex:indexPath.row];
    self.apiKeyTextField.text = apiKey;
    
    // Add the selected apikey
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:apiKey forKey:@"lastApiKey"];
    [userDefaults synchronize];
}

#pragma mark - QRCodeReader Delegate Methods

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result {
    [reader stopScanning];
    [self dismissViewControllerAnimated:YES completion:^{
        UIAlertController * alert = [UIAlertController
            alertControllerWithTitle:@"Scanned string"
            message:result
            preferredStyle:UIAlertControllerStyleAlert
         ];

        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {}];

        [alert addAction:yesButton];

        [self presentViewController:alert animated:YES completion:nil];
    }];
    
    NSString *pattern = @"naoApikey\\s*:\\s*(.+?)(?=,|$)";
    self.apiKeyTextField.text = [self extract:result withRegex:pattern];
}

- (NSString *)extract:(NSString *)text withRegex:(NSString *)pattern{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                           options:0 error:NULL];
    NSTextCheckingResult *match = [regex firstMatchInString:text options:0 range:NSMakeRange(0, [text length])];
    NSString *result;
    if (match != nil) {
        result = [text substringWithRange:[match rangeAtIndex:1]];
    }
    return result;
}

- (IBAction)onScanApikeyButtonPressed:(UIButton *)sender {
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
        static QRCodeReaderViewController *vc = nil;
        static dispatch_once_t onceToken;

        dispatch_once(&onceToken, ^{
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            vc                   = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:YES showTorchButton:YES];
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
        });
        vc.delegate = self;

        [vc setCompletionWithBlock:^(NSString *resultAsString) {}];
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else {
        UIAlertController * alert = [UIAlertController
        alertControllerWithTitle:@"Error"
        message:@"Reader not supported by the current device"
        preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Yes, please"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {}];

        [alert addAction:yesButton];

        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader {
  [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - NSUserDefaults addint the apikey into the pref

- (void)appendApikeyIntoPref:(NSString *)apikey {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *arrayOfApikeys = [[userDefaults stringArrayForKey:@"arrayOfApikeys"] mutableCopy];
    if (arrayOfApikeys != nil && ![arrayOfApikeys containsObject: apikey]){
        [arrayOfApikeys addObject:apikey];
    }
    else{
        arrayOfApikeys = [[NSMutableArray alloc] initWithObjects:apikey,nil];
    }
    
    [userDefaults setObject:apikey forKey:@"lastApiKey"];
    [userDefaults setObject:apikey forKey:@"apiKey"];
    [userDefaults setObject:arrayOfApikeys forKey:@"arrayOfApikeys"];
    [userDefaults synchronize];
}


+ (NSString *)getApikeyFromPref {
    NSString *apiKey = [[NSUserDefaults standardUserDefaults] objectForKey:@"apiKey"];
    NSString *lastApiKey = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastApiKey"];
    
    return (lastApiKey == nil)? apiKey : lastApiKey;
}

- (NSMutableArray *)getApikeyListFromPref {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *arrayOfApikeys = [[userDefaults stringArrayForKey:@"arrayOfApikeys"] mutableCopy];
    return arrayOfApikeys;
}

- (BOOL)search:(NSString *)apikey {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *arrayOfApikeys = [[userDefaults stringArrayForKey:@"arrayOfApikeys"] mutableCopy];
    return [arrayOfApikeys containsObject: apikey];
}
// Refresh the tableview
- (void) refresh {
    self.apiKeyList = [self getApikeyListFromPref];
    
    [self.apiKeySelectionTableView reloadData];
}
@end
