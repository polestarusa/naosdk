//
//  ProgressBarView.h
//  NAODemoApplication
//
//  Created by Pole Star on 10/02/2022.
//  Copyright © 2022 David FERNANDEZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MAX_SPEED 360

NS_ASSUME_NONNULL_BEGIN

@interface ProgressBarView : UIView
    @property CGFloat startAngle;
    @property CGFloat endAngle;
    @property NSDictionary *attributes;
    @property int percent;
@end

NS_ASSUME_NONNULL_END
