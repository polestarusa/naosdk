//
//  SpeedUIViewController.m
//  NAODemoApplication
//
//  Created by Pole Star on 10/02/2022.
//  Copyright © 2022 David FERNANDEZ. All rights reserved.
//
#import "ProgressBarView.h"
#import "SpeedUIViewController.h"
#import "ApiKeySelectionViewController.h"

@interface SpeedUIViewController ()

@end

@implementation SpeedUIViewController {
    NSString *apiKey;
    UIActivityIndicatorView *spinner;
    BOOL startButtonState;
    BOOL stopButtonState;
    int keyboardHeight;
}

- (void)viewDidLoad {
    // Init our view
    [super viewDidLoad];
    self.m_testView = [[ProgressBarView alloc] initWithFrame:self.view.bounds];
    self.m_testView.percent = 0;
    [self.view addSubview:self.m_testView];
    
    apiKey = [ApiKeySelectionViewController getApikeyFromPref];
    
    if (self.locationHandle == nil) {
        [self startWaiting];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.locationHandle = [[NAOLocationHandle alloc] initWithKey:apiKey delegate:self sensorsDelegate:self];
                [self.locationHandle synchronizeData:self];
                [self performSelectorOnMainThread:@selector(stopWaiting) withObject:nil waitUntilDone:YES];
            });
        });
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    // Kick off a timer to count it down
//    self.m_timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(decrementSpin) userInfo:nil repeats:YES];
}

- (void)decrementSpin {
    // If we can decrement our percentage, do so, and redraw the view
    if (self.m_testView.percent <=  MAX_SPEED) {
        self.m_testView.percent = self.m_testView.percent + 1;
        [self.m_testView setNeedsDisplay];
    }
    else {
       [self.m_timer invalidate];
        self.m_timer = nil;
    }
}

- (void)startWaiting {
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    spinner.hidesWhenStopped = YES;
    [self.view addSubview:spinner];
    [spinner startAnimating];
}

- (void)stopWaiting {
    [spinner stopAnimating];
    [spinner removeFromSuperview];
}

#pragma mark - NAOLocationHandleDelegate

- (void) didLocationChange:(CLLocation *)location {
    dispatch_async(dispatch_get_main_queue(), ^{
//        NSLog(@"############# [%d]\t%s\tSpeed: %f", __LINE__, __FILE__, location.speed);
        self.m_testView.percent = location.speed * 3.6;
        [self.m_testView setNeedsDisplay];
    });
}

- (void) didFailWithErrorCode:(DBNAOERRORCODE)errCode andMessage:(NSString *)message {
    //[self enableStartButton];
    
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
}

- (void) didLocationStatusChanged:(DBTNAOFIXSTATUS)status {
    NSString* statusString = [self stringFromStatus:status];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), statusString);
}


- (NSString*) stringFromStatus:(DBTNAOFIXSTATUS) status {
    switch (status) {
        case DBTNAOFIXSTATUS_NAO_FIX_UNAVAILABLE:
            return @"FIX_UNAVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_FIX_AVAILABLE:
            return @"AVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_TEMPORARY_UNAVAILABLE:
            return @"TEMPORARY_UNAVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_OUT_OF_SERVICE:
            return @"OUT_OF_SERVICE";
            break;
        default:
            return @"UNKNOWN";
            break;
    }
}

- (void) didEnterSite:(NSString *)name {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), name);

}

- (void) didExitSite:(NSString *)name {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), name);
}

#pragma mark - NAOSyncDelegate

- (void)  didSynchronizationSuccess {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    [self stopWaiting];
    [self.locationHandle start];
}

- (void)  didSynchronizationFailure:(DBNAOERRORCODE)errorCode msg:(NSString*) message {
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errorCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
    [self stopWaiting];
}

#pragma mark - NAOSensorsDelegate

- (void)requiresBLEOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresWifiOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresLocationOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresCompassCalibration {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

//Optional
- (void)didCompassCalibrated {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}


@end
