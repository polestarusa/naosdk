//
//  TrackingServiceViewController.h
//  NAODemoApplication
//
//  Created by Pole Star on 10/09/2020.
//  Copyright © 2020 David FERNANDEZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NAOSDK/NAOSDK.h>
NS_ASSUME_NONNULL_BEGIN

@interface TrackingServiceViewController : UIViewController <UITextFieldDelegate, NAOSyncDelegate, NAOTrackingHandleDelegate, NAOSensorsDelegate>
@property NAOTrackingHandle *trackingHandle;
@property (weak, nonatomic) IBOutlet UITextView *consoleTextView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *synchronizeButton;

@property (strong, nonatomic) NSString *apikey;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UITextField *assetIdTextField;

@end

NS_ASSUME_NONNULL_END
