//
//  SpeedUIViewController.h
//  NAODemoApplication
//
//  Created by Pole Star on 10/02/2022.
//  Copyright © 2022 David FERNANDEZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NAOSDK/NAOSDK.h>
@class ProgressBarView;

NS_ASSUME_NONNULL_BEGIN

@interface SpeedUIViewController : UIViewController<NAOLocationHandleDelegate, NAOSyncDelegate, NAOSensorsDelegate>
    @property ProgressBarView* m_testView;
    @property NSTimer* m_timer;
    @property NAOLocationHandle* locationHandle;
@end

NS_ASSUME_NONNULL_END
