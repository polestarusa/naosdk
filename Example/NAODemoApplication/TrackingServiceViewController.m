//
//  TrackingServiceViewController.m
//  NAODemoApplication
//
//  Created by Pole Star on 10/09/2020.
//  Copyright © 2020 David FERNANDEZ. All rights reserved.
//

#import "TrackingServiceViewController.h"
#import "ApiKeySelectionViewController.h"


static UIColor *disabled_widget_color = nil;
static UIColor *enabled_widget_color = nil;
static UIColor *textview_background_color = nil;
static UIColor *textview_color = nil;


@interface TrackingServiceViewController ()

@end

@implementation TrackingServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    disabled_widget_color = [UIColor colorWithRed: 0.92 green: 0.92 blue: 0.89 alpha: 1.00];
    enabled_widget_color = [UIColor colorWithRed: 0.26 green: 0.53 blue: 0.96 alpha: 1.00];
    
    
    textview_background_color =  [UIColor colorWithRed:76.0/255.0
                                                 green:76.0/255.0
                                                  blue:76.0/255.0
                                                 alpha:1.0];
                                                                                                           
    textview_color =  [UIColor colorWithRed: 0.78
                                      green: 0.78
                                       blue: 0.80
                                      alpha: 1.00];
    
    [self.assetIdTextField setDelegate:self];
    self.assetIdTextField.layer.masksToBounds=YES;
    self.assetIdTextField.layer.cornerRadius = 5.f;
    self.assetIdTextField.layer.borderWidth= 1.0f;
    
    [self.assetIdTextField setBackgroundColor:textview_color];
    self.assetIdTextField.layer.borderColor=[textview_background_color CGColor];

    
    self.startButton.layer.cornerRadius = 5;
    self.stopButton.layer.cornerRadius = 5;
    self.synchronizeButton.layer.cornerRadius = 5;
    
    self.consoleTextView.editable = NO;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.apikey = [ApiKeySelectionViewController getApikeyFromPref];
    
    if (self.trackingHandle == nil) {
        self.trackingHandle = [[NAOTrackingHandle alloc] initWithKey:self.apikey delegate:self sensorsDelegate:self];
    }
    
    [self.versionLabel setText:[NSString stringWithFormat:@"Version : %@", [NAOServicesConfig getSoftwareVersion]]];
    
    [self disable:self.stopButton];
    [self enable:self.synchronizeButton];
    [self disable:self.startButton];
//    [self.assetIdTextFieald setText:[[UIDevice currentDevice] name]];
}

-(void)dismissKeyboard {
    [self.assetIdTextField resignFirstResponder];
}

- (IBAction)onStartButtonClicked:(UIButton *)sender {
    [self enable:self.stopButton];
    @try {
        [self.trackingHandle start];
        if (self.assetIdTextField.text.length > 0 || self.assetIdTextField.text != nil || ![self.assetIdTextField.text isEqual:@""]){
//            [self setUserIdentifier];
            [[NAOServicesConfig  getNaoContext] setIdentifier: self.assetIdTextField.text witUserConsent:YES];
//            NSLog(@"################\t[%d]\t%s\t%s",__LINE__, __FILE__, __func__);
        }
        [self appendMsg:@"Start ..."];
        [self disable:self.startButton];
        [self enable:self.stopButton];
//        NSLog(@"################\t[%d]\t%s\t%s",__LINE__, __FILE__, __func__);
        //    [[NAOServicesConfig  getNaoContext] setIDentifier: hhh]
    }
    @catch(id anException) {}
    
//    NSLog(@"################\t[%d]\t%s\t%s",__LINE__, __FILE__, __func__);
}

- (void)setUserIdentifier {
    
    [self dismissViewControllerAnimated:YES completion:^{
        UIAlertController * alert = [UIAlertController
            alertControllerWithTitle:@"User identifier setting!"
            message:@"Do you give your consent to this identifier for the NAO SDK tracking?"
            preferredStyle:UIAlertControllerStyleAlert
         ];

        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"YES"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
            [[NAOServicesConfig  getNaoContext] setIdentifier: self.assetIdTextField.text witUserConsent:YES];
        }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"NO"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Continue
                                   }];

        [alert addAction:yesButton];
        [alert addAction:noButton];

        [self presentViewController:alert animated:YES completion:nil];
    }];
}

- (IBAction)onStopButtonClicked:(UIButton *)sender {
    [self enable:self.startButton];
    [self.trackingHandle stop];
//    NSLog(@"################\t[%d]\t%s",__LINE__, __FILE__);
}

- (IBAction)onSynchronizeButtonClicked:(UIButton *)sender {
    [self waitForSynchro];
    [self.trackingHandle synchronizeData:self];
//    NSLog(@"################\t[%d]\t%s",__LINE__, __FILE__);
}

- (void)enable:(UIButton *)button {
    button.enabled = YES;
    button.backgroundColor = enabled_widget_color;
//    NSLog(@"################\t[%d]\t%s",__LINE__, __FILE__);
}

- (void)disable:(UIButton *)button {
    button.enabled = NO;
    button.backgroundColor = disabled_widget_color;
//    NSLog(@"################\t[%d]\t%s",__LINE__, __FILE__);
}

- (void)appendMsg:(NSString *) msg{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *line = [NSString stringWithFormat:@"$ [%@]\t%@\n", [dateFormatter stringFromDate:[NSDate date]], msg];
    [self.consoleTextView setText:line];
}

- (void)waitForSynchro {
    [self disable: self.startButton];
    [self disable: self.synchronizeButton];
    [self enable: self.stopButton];
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    self.spinner.hidesWhenStopped = YES;
    [self.view addSubview:self.spinner];
    [self.spinner startAnimating];
}

- (void)dismissAfterSynchro {
    [self enable: self.synchronizeButton];
    [self.spinner stopAnimating];
}

#pragma mark - NAOAnalyticsHandleDelegate

- (void) didFailWithErrorCode:(DBNAOERRORCODE)errCode andMessage:(NSString *)message {
    [self enable:self.startButton];
    
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
    [self appendMsg:errorText];
}

#pragma mark - NAOSensorsDelegate

- (void) requiresBLEOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void) requiresWifiOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void) requiresLocationOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void) requiresCompassCalibration {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

#pragma mark - NAOSyncDelegate

- (void)  didSynchronizationSuccess {
    [self enable:self.startButton];
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    NSString *errorText = [NSString stringWithFormat:@"%@", NSStringFromSelector(_cmd)];
    [self appendMsg:errorText];
    [self dismissAfterSynchro];
}

- (void)  didSynchronizationFailure:(DBNAOERRORCODE)errorCode msg:(NSString*) message {
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errorCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
    [self appendMsg:errorText];
    [self dismissAfterSynchro];
}

#pragma mark - TextField delegate
// UITextFieldDelegate to hide keyboard when user clicks on enter/done
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField {
    [self.assetIdTextField setEnabled:NO];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *characters = [textField.text stringByReplacingCharactersInRange:range withString:string];

    int minimumCharacter = 3;

    //User identifier
    if (textField == self.assetIdTextField) {
        if  (characters.length >= minimumCharacter) {
            [self enable: self.startButton];
        }
        else {
            [self disable: self.startButton];
        }
    }

    return YES;
}
@end
