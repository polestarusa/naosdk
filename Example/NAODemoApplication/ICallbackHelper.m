//
//  ICallbackHelper.m
//  NAODemoApplication
//
//  Created by Pole Star on 27/02/2020.
//  Copyright © 2020 Pole Star. All rights reserved.
//

#import "ICallbackHelper.h"

@implementation ICallbackHelper

- (void)onSuccess:(nonnull NSString *)msg{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
        UIAlertController * alertController = [UIAlertController
                alertControllerWithTitle:@"Upload NAO logs info scheduled"
                                 message:@"Success"
                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [controller presentViewController:alertController animated:YES completion:nil];
    });
    
}

- (void)onFailure:(nonnull NSString *)msg{
    UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIAlertController * alertController = [UIAlertController
            alertControllerWithTitle:@"Upload NAO logs info scheduled"
                             message:msg
                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:ok];
    
    [controller presentViewController:alertController animated:YES completion:nil];
}

@end
