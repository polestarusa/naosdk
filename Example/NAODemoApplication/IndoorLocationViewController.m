//
//  IndoorLocationViewController.m
//  NAODemoApplication
//
//  Created by Pole Star on 27/11/2015.
//  Copyright © 2015 Pole Star. All rights reserved.
//

#import "IndoorLocationViewController.h"
#import "ApiKeySelectionViewController.h"

@implementation IndoorLocationViewController {
    NSString *apiKey;
    UIActivityIndicatorView *spinner;
    BOOL startButtonState;
    BOOL stopButtonState;
    int keyboardHeight;
}

@synthesize locationHandle;
@synthesize statusLabel, locationLabel;

static UIColor *disabled_widget_color = nil;
static UIColor *enabled_widget_color = nil;
static UIColor *textview_background_color = nil;
static UIColor *textview_color = nil;

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    disabled_widget_color = [UIColor colorWithRed: 0.92 green: 0.92 blue: 0.89 alpha: 1.00];
    enabled_widget_color = [UIColor colorWithRed: 0.26 green: 0.53 blue: 0.96 alpha: 1.00];
    
    textview_background_color =  [UIColor colorWithRed:76.0/255.0
                                                 green:76.0/255.0
                                                  blue:76.0/255.0
                                                 alpha:1.0];
                                                                                                           
    textview_color =  [UIColor colorWithRed: 0.78
                                      green: 0.78
                                       blue: 0.80
                                      alpha: 1.00];
    
    self.startButton.layer.cornerRadius = 5;
    self.stopButton.layer.cornerRadius = 5;
    self.synchronizeButton.layer.cornerRadius = 5;
    self.enableAutoPdbSyncButton.layer.cornerRadius = 5;
    self.setExternalLocButton.layer.cornerRadius = 5;
    self.lastLocationButton.layer.cornerRadius = 5;
    self.lastLocStackView.layer.cornerRadius = 5;
    self.restartButton.layer.cornerRadius = 5;
    
    self.locationLabel.layer.masksToBounds=YES;
    self.locationLabel.layer.cornerRadius = 5.f;
    self.locationLabel.layer.borderWidth= 1.0f;
    
    [self.locationLabel setBackgroundColor:textview_color];
    self.locationLabel.layer.borderColor=[textview_color CGColor];
    [self.lastLocStackView setHidden:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    apiKey = [ApiKeySelectionViewController getApikeyFromPref];
        
    if (self.locationHandle == nil) {
        [self startWaiting];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.locationHandle = [[NAOLocationHandle alloc] initWithKey:apiKey delegate:self sensorsDelegate:self];
                [self performSelectorOnMainThread:@selector(stopWaiting) withObject:nil waitUntilDone:YES];
            });
        });
    }
    
    [self resetErrorLabel];
    
    [self.naosdkVersionLabel setText:[NSString stringWithFormat:@"Version : %@", [NAOServicesConfig getSoftwareVersion]]];
    
    self.notificationManager = [[NotificationManager alloc] init];
    
    stopButtonState = NO;
    startButtonState = YES;
    [self.stopButton setEnabled:stopButtonState];
    
    [self disable:self.stopButton];
    [self enable:self.synchronizeButton];
//    [self disable:self.startButton];
//    [self disable:self.restartButton];
    
    //KeyBoard size on a iPhone SE
    keyboardHeight = 253;
    self.delaiNotificationTextField.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)dismissKeyboard {
    [self.delaiNotificationTextField resignFirstResponder];
}

- (IBAction)onGetLastLocationBtnPressed:(UIButton *)sender {
    //self.lastLocConsoleLabel.text = @"Last location";
    [self.lastLocStackView setHidden:NO];
//    CLLocation *location = [self.locationHandle getLastKnownLocation];
    CLLocation *location = [[NAOServicesConfig getNaoContext] getLastKnownLocation];
    self.lastLocConsoleLabel.text = [NSString stringWithFormat:@"%.6f, %.6f, %.3f, %.2f, HozAccuracy : %.2f, VerAccuracy : %.2f", location.coordinate.longitude, location.coordinate.latitude, location.altitude, location.course, location.horizontalAccuracy,location.verticalAccuracy];
}

-(void)keyboardWillShow:(NSNotification*)notification {
    // Animate the current view out of the way
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    keyboardHeight = keyboardFrameBeginRect.size.height;
}

-(void)keyboardWillHide:(NSNotification*)notification {
    [self animateTextField:self.selectedTextView up: NO];
}

-(void) viewDidAppear:(BOOL)animated {
    
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController || self.isBeingDismissed) {
        [self stopLoc];
    }
}

- (IBAction)stubModeSwitch:(id)sender {
    if (self.locationHandle != nil) {
        [self stopLoc];
    }
    
    [self resetErrorLabel];
    
    if ([sender isOn]) {
        self.locationHandle = [[NAOLocationHandle alloc] initWithKey:@"emulator" delegate:self sensorsDelegate:self];
    } else {
        self.locationHandle = [[NAOLocationHandle alloc] initWithKey:apiKey delegate:self sensorsDelegate:self];
        [self.locationHandle synchronizeData:self];
    }
}

- (IBAction)enableAutoPdbSync:(id)sender {
    [[NAOServicesConfig getNaoContext] synchronizeData:self apiKey:apiKey enableAutoPdbSync:YES];
}

- (IBAction)setExternalLocButton:(id)sender {
    CLLocation *dummyLoc = [[CLLocation alloc] initWithLatitude:40.0f longitude:40.0f];
    
    CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(40.0f, 40.0f);
    float magDecl = 0.0f;
    dummyLoc = [[CLLocation alloc] initWithCoordinate:coords altitude:0.0f horizontalAccuracy:100.0f verticalAccuracy:100.0f course:45.0f speed:magDecl timestamp:[NSDate date]];
    
    NSArray *locations = @[dummyLoc];
    [[NAOServicesConfig getNaoContext] setExternalLocation: locations];
}

- (IBAction)Start:(id)sender {
    //[self enableStopButton];
    [self enable:self.stopButton];
    [self.locationHandle start];
}

- (IBAction)stop:(id)sender {
    //[self enableStartButton];
    
    [self stopLoc];
    [self enable:self.startButton];
    [self enable:self.restartButton];
}

- (IBAction)SynchronizeButtonClicked:(id)sender {
    [self startWaiting];
    [self.locationHandle synchronizeData:self];
}

- (IBAction)onRestartButtonPressed:(UIButton *)sender {
    if (self.locationHandle != nil) {
        [self enable:self.stopButton];
        [self.locationHandle restart];
    }
}


- (IBAction)launchScheduledNotification:(id)sender {
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:self.delaiNotificationTextField.text];
    
    if (myNumber) {
        [self.notificationManager displayNotificationWithMessage:@"Scheduled notification" withTimer:myNumber];
    }
}

- (void)stopLoc {
    [self.locationHandle stop];
    //locationLabel.text = @"Location:longitude, latitude, altitude, heading";
    //statusLabel.text = @"Status: status";
}

- (void)resetErrorLabel {
    [self.errorConsoleLabel setText:@""];
    [self.errorConsoleLabel setHidden:YES];
}

- (void)startWaiting {
    [self.startButton setEnabled:NO];
    [self.stopButton setEnabled:NO];
    [self.synchronizeButton setEnabled:NO];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    spinner.hidesWhenStopped = YES;
    [self.view addSubview:spinner];
    [spinner startAnimating];
}

- (void)stopWaiting {
    [self.startButton setEnabled:startButtonState];
    [self.stopButton setEnabled:stopButtonState];
    [self.synchronizeButton setEnabled:YES];
    
    [spinner stopAnimating];
    [spinner removeFromSuperview];
}

- (void)onEnterBackground {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    if (self.locationHandle != nil) {
        [self.locationHandle setPowerMode:DBTPOWERMODE_LOW];
    }
}

- (void)onEnterForeground {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    if (self.locationHandle != nil) {
        [self.locationHandle setPowerMode:DBTPOWERMODE_HIGH];
    }
}

- (void)enable:(UIButton *)button {
    button.enabled = YES;
    button.backgroundColor = enabled_widget_color;
//    NSLog(@"################\t[%d]\t%s",__LINE__, __FILE__);
}

- (void)disable:(UIButton *)button {
    button.enabled = NO;
    button.backgroundColor = disabled_widget_color;
//    NSLog(@"################\t[%d]\t%s",__LINE__, __FILE__);
}

#pragma mark - NAOLocationHandleDelegate

- (void) didLocationChange:(CLLocation *)location {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.locTypeLabel.text = @"Current location:";
        self.locationLabel.text = [NSString stringWithFormat:@"%.6f, %.6f, %.3f, %.2f, HozAccuracy : %.2f, VerAccuracy : %.2f, speed : %.2f m/s", location.coordinate.longitude, location.coordinate.latitude, location.altitude, location.course, location.horizontalAccuracy,location.verticalAccuracy,location.speed];
    });
}

- (void) didFailWithErrorCode:(DBNAOERRORCODE)errCode andMessage:(NSString *)message {
    //[self enableStartButton];
    
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
    [self.notificationManager displayNotificationWithMessage:[NSString stringWithFormat:@"%@ : %@", NSStringFromSelector(_cmd), errorText]];
    
    [self.errorConsoleLabel setText:errorText];
    [self.errorConsoleLabel setHidden:NO];
}

- (void) didLocationStatusChanged:(DBTNAOFIXSTATUS)status {
    NSString* statusString = [self stringFromStatus:status];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), statusString);
    [self.notificationManager displayNotificationWithMessage:[NSString stringWithFormat:@"%@ : %@", NSStringFromSelector(_cmd), statusString]];

    dispatch_async(dispatch_get_main_queue(), ^{
        self.statusLabel.text = [NSString stringWithFormat:@"%@", statusString];
    });
}


- (NSString*) stringFromStatus:(DBTNAOFIXSTATUS) status {
    switch (status) {
        case DBTNAOFIXSTATUS_NAO_FIX_UNAVAILABLE:
            return @"FIX_UNAVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_FIX_AVAILABLE:
            return @"AVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_TEMPORARY_UNAVAILABLE:
            return @"TEMPORARY_UNAVAILABLE";
            break;
        case DBTNAOFIXSTATUS_NAO_OUT_OF_SERVICE:
            return @"OUT_OF_SERVICE";
            break;
        default:
            return @"UNKNOWN";
            break;
    }
}

- (void) didEnterSite:(NSString *)name {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), name);
    [self.notificationManager displayNotificationWithMessage:[NSString stringWithFormat:@"%@ : %@", NSStringFromSelector(_cmd), name]];
}

- (void) didExitSite:(NSString *)name {
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), name);
    [self.notificationManager displayNotificationWithMessage:[NSString stringWithFormat:@"%@ : %@", NSStringFromSelector(_cmd), name]];
}

#pragma mark - NAOSyncDelegate

- (void)  didSynchronizationSuccess {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    [self.notificationManager displayNotificationWithMessage:[NSString stringWithFormat:@"%@", NSStringFromSelector(_cmd)]];
    
    [self resetErrorLabel];
    [self stopWaiting];

    [self enable:self.startButton];
    [self enable:self.restartButton];
}

- (void)  didSynchronizationFailure:(DBNAOERRORCODE)errorCode msg:(NSString*) message {
    NSString *errorText = [NSString stringWithFormat:@"errorCode:%ld  message:%@", (long)errorCode, message];
    NSLog(@"NAODemoApp : %@ : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), errorText);
    [self.notificationManager displayNotificationWithMessage:[NSString stringWithFormat:@"%@ : %@", NSStringFromSelector(_cmd), errorText]];

    [self.errorConsoleLabel setText:errorText];
    [self.errorConsoleLabel setHidden:NO];
    [self stopWaiting];
}

#pragma mark - NAOSensorsDelegate

- (void)requiresBLEOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresWifiOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresLocationOn {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

- (void)requiresCompassCalibration {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

//Optional
- (void)didCompassCalibrated {
    NSLog(@"NAODemoApp : %@ : %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.selectedTextView = textField;
    [self animateTextField: textField up: YES];
    return YES;
}

// It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    
    int botTF = textField.frame.origin.y + textField.frame.size.height;
    int heightLeft = self.view.frame.size.height - botTF;
    int movementDistance = keyboardHeight - heightLeft;
    
    if (movementDistance < 0) {
        movementDistance = 0;
    }
    
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

@end
